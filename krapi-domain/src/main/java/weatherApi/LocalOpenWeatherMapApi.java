package weatherApi;

import java.time.LocalDateTime;

import org.Krapi.domain.interfeesjes.IApiConverter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import domain.Forecast;
import domain.Observation;

public class LocalOpenWeatherMapApi implements IApiConverter
{
	public static double Kelvin = 273.15;
	
	public Observation getData(JSONObject data)
	{
		JSONParser parser = new JSONParser();
		String shizzle = (String)data.get("dataString");
		try
		{
			JSONObject value = (JSONObject) parser.parse(shizzle);
			JSONObject levels = (JSONObject) ((JSONObject) ((JSONArray) value.get("list")).get(1)).get("main");
			int temp = new Integer((int) ((Double)levels.get("temp")-Kelvin));
			JSONObject city = (JSONObject) value.get("city");
			String location = (String)city.get("name");
			
			System.out.println("Location: " + location + " ; Temperature: " + temp + "°C");
			
			return new Observation(LocalDateTime.now(), location, temp);
		}
		catch(ParseException e)
		{
			
		}
		return null;
	}

	public Forecast getForecast(JSONObject data) {
		// TODO Auto-generated method stub
		return null;
	}

}
