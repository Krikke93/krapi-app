package weatherApi;

import java.time.LocalDateTime;

import org.Krapi.domain.interfeesjes.IApiConverter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import domain.Forecast;
import domain.Observation;

public class GlobalOpenWeatherMapApi implements IApiConverter
{
	public Observation getData(JSONObject data)
	{
		JSONObject value = new JSONObject();
		JSONObject cityvalue = new JSONObject();
		JSONArray listvalue = new JSONArray();

		JSONParser parser = new JSONParser();
		String shizzle = (String)data.get("dataString");
		try
		{
			value = (JSONObject) parser.parse(shizzle);
			cityvalue = (JSONObject)parser.parse(value.get("city").toString());
			listvalue = (JSONArray)parser.parse(value.get("list").toString());
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		
		String location = cityvalue.get("name") + ", " + cityvalue.get("country");
		return new Observation(LocalDateTime.now(), location, 15);
	}
	
	public Forecast getForecast(JSONObject obj)
	{
		return null;
	}
}