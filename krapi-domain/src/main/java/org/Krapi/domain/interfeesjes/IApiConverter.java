package org.Krapi.domain.interfeesjes;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import domain.Forecast;
import domain.Observation;

public interface IApiConverter
{
	Observation getData(JSONObject data);
	Forecast getForecast(JSONObject data);
}
