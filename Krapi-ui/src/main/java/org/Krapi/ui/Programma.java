package org.Krapi.ui;

import java.util.List;

import javax.swing.JOptionPane;

import org.Krapi.domain.interfeesjes.IForecastService;

import domain.Forecast;
import domain.Observation;
import service.ForecastSerivce;

public class Programma {
	
	public static void main(String[] argumentjes)
	{
		IForecastService service = new ForecastSerivce();
		
		String input = JOptionPane.showInputDialog("Land (bv. BE)");
		Observation obs = service.getCurrentObservation(input);
		System.out.println(obs.getLocation() + ", " + obs.getTemperature() + "°C, " + obs.getTime());
	}
}
