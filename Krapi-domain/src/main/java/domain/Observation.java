package domain;

import java.time.LocalDateTime;

public class Observation {

	LocalDateTime time;
	String location;
	int temperature;
	
	public Observation(LocalDateTime time, String location, int temperature)
	{
		setTime(time);
		setLocation(location);
		setTemperature(temperature);
	}
	
	private void setTemperature(int temperature)
	{
		this.temperature = temperature;
	}

	private void setLocation(String location)
	{
		this.location = location;
	}

	private void setTime(LocalDateTime time)
	{
		this.time = time;
	}
	
	public LocalDateTime getTime()
	{
		return time;
	}
	
	public String getLocation()
	{
		return location;
	}
	
	public int getTemperature()
	{
		return temperature;
	}
	
}
