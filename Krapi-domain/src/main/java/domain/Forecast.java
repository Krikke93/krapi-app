package domain;

import java.time.LocalDateTime;
import java.util.Date;

public class Forecast {
	
	LocalDateTime time;
	String location;
	Date forecastDate;
	int maximumtemperature;
	
	public LocalDateTime getTime() {
		return time;
	}
	public void setTime(LocalDateTime time) {
		this.time = time;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Date getForecastDate() {
		return forecastDate;
	}
	public void setForecastDate(Date forecastDate) {
		this.forecastDate = forecastDate;
	}
	public int getMaximumtemperature() {
		return maximumtemperature;
	}
	public void setMaximumtemperature(int maximumtemperature) {
		this.maximumtemperature = maximumtemperature;
	}

	
}
