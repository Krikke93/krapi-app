package org.Krapi.domain.interfeesjes;

import java.util.List;

import domain.Forecast;
import domain.Observation;

public interface IForecastService 
{
	void loadWeather(String apikey);
	Observation getCurrentObservation(String location);
	List<Forecast> getForecast(String location);
}