package service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.simple.JSONObject;

public class NetLoader 
{
	public JSONObject load(String urlie)
	{
		try
		{
			URL url = new URL(urlie);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept",  "application/json");
			conn.connect();
			if(conn.getResponseCode() != 200)
				throw new RuntimeException("Failed: HTTP error code: " + conn.getResponseCode());
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			String output;
			String toReturn = "";
			while((output = reader.readLine()) != null)
			{
				toReturn += output;
			}
			JSONObject a = new JSONObject();
			a.put("dataString", toReturn);
			conn.disconnect();
			return a;
		}
		catch(MalformedURLException e)
		{
			
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
		
	}
}
